<?php


namespace User\Domain\Service\User;


use User\Application\User\Response\LoginResponse;
use User\Domain\Model\User\LoginRequest;
use User\Domain\Model\User\UserRepository;

class LoginService
{
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var LoginResponse
     */
    private $response;

    public function __construct(UserRepository $repository, LoginResponse $response)
    {
        $this->repository = $repository;
        $this->response = $response;
    }

    public function execute(LoginRequest $request): LoginResponse
    {
        $user = $this->repository->getLoginUser($request->getEmail(), $request->getPassword());

        return $this->response->populate($user);

    }

}