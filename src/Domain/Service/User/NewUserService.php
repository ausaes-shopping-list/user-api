<?php

namespace User\Domain\Service\User;

use User\Domain\Model\User\NewUserRequest;
use User\Domain\Model\User\User;
use User\Domain\Model\User\UserRepository;

class NewUserService
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(NewUserRequest $newUserRequest)
    {
        $user = new User($newUserRequest->getEmail(), $newUserRequest->getPassword(), $newUserRequest->getUsername(), $this->getToken(($newUserRequest)));
        $this->repository->addUser($user);
    }

    private function getToken(NewUserRequest $request)
    {
        return sha1(md5($request->getEmail() . $request->getPassword()));
    }
}