<?php


namespace User\Domain\Model\User;


class LoginResult
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $username;

    private function __construct(string $username, string $email, string $token)
    {
        $this->username = $username;
        $this->email = $email;
        $this->token = $token;
    }

    public static function createFromUser(User $user): self
    {
        return new self($user->getUsername(), $user->getEmail(), $user->getToken());
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

}