<?php

namespace User\Domain\Model\User;

class User
{

    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $token;

    public function __construct(
        string $email,
        string $password,
        string $username,
        string $token
    ) {
        $this->setId(uniqid());
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setUsername($username);
        $this->setToken($token);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    private function setId(string $id)
    {
        $this->id = $id;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setPassword(string $password): void
    {

        $this->password = $password;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }


}