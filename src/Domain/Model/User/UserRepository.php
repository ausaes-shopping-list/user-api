<?php

namespace User\Domain\Model\User;

interface UserRepository
{
    public function addUser(User $user): void;
    public function modUser(User $user): void;
    public function getLoginUser(string $email, string $password): User;

}