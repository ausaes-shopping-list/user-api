<?php

namespace User\Infrastructure\UserRepository;

use Exception;
use MongoDB\Client;
use MongoDB\Collection;
use phpDocumentor\Reflection\Types\Object_;
use User\Domain\Model\User\User;
use User\Domain\Model\User\UserRepository;

class MongoUserRepository implements UserRepository
{

    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $collectionName;
    /**
     * @var string
     */
    private $databaseName;
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(string $schema, string $databaseName, string $collectionName)
    {
        $this->client = new Client($schema);
        $this->collectionName = $collectionName;
        $this->databaseName = $databaseName;
        $this->collection = $this->client->{$this->databaseName}->{$this->collectionName};
    }

    public function addUser(User $user): void
    {
        if ($this->getUserByEmail($user->getEmail())) {
            throw new \Exception('email already exists');
        }

        $this->collection->insertOne([
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'password' => password_hash($user->getPassword(), PASSWORD_DEFAULT),
            'token' => $user->getToken()
        ]);


    }

    public function modUser(User $user): void
    {
        // TODO: Implement modUser() method.
    }

    /**
     * @param string $email
     * @param string $password
     * @return User
     * @throws Exception
     */
    public function getLoginUser(string $email, string $password): User
    {
        $mongoUser = $this->getUserByEmail($email);
        if (empty($mongoUser)) {
            throw new Exception("user {$email} not found");
        }
        if (!password_verify($password, $mongoUser['password'])) {
            throw new Exception('wrong password');
        }

        return new User(
            $mongoUser['email'],
            $mongoUser['password'],
            $mongoUser['username'],
            $mongoUser['token']
        );
    }

    private function getUserByEmail(string $email)
    {
        return $this->collection->findOne([
            'email' => $email
        ]);
    }
}