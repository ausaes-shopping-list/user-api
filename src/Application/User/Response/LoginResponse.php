<?php


namespace User\Application\User\Response;


use User\Domain\Model\User\User;

class LoginResponse
{
    /**
     * @var User
     */
    private $user;

    public function populate(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->user->getUsername();
    }

    public function getEmail(): string
    {
        return $this->user->getEmail();
    }

    public function getToken(): string
    {
        return $this->user->getToken();
    }

}