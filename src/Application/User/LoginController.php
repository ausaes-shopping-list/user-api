<?php

namespace User\Application\User;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use User\Application\User\Request\GetLoginRequest;
use User\Domain\Service\User\LoginService;

class LoginController
{
    /**
     * @var LoginService
     */
    private $service;
    /**
     * @var GetLoginRequest
     */
    private $request;
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(GetLoginRequest $request, LoginService $loginService, SerializerInterface $serializer)
    {
        $this->service = $loginService;
        $this->request = $request;
        $this->serializer = $serializer;
    }

    public function login(Request $request)
    {
        try {
            $loginRequest = $this->request->build($request->request->all());
            $response = $this->service->execute($loginRequest);
            return new JsonResponse(
                $this->serializer->serialize($response, 'json'),
                200,
                ['Content-Type' => 'application/json; charset=UTF-8'],
                true
            );
        } catch (Exception $e) {
            return new JsonResponse(json_encode(['error' => $e->getMessage()]), 500, ['Content-Type' => 'application/json; charset=UTF-8'], true);
        }


    }

}