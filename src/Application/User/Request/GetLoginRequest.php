<?php


namespace User\Application\User\Request;


use Exception;
use User\Domain\Model\User\LoginRequest;

class GetLoginRequest
{

    /**
     * @param array $data
     * @return LoginRequest
     * @throws Exception
     */
    public function build(array $data): LoginRequest
    {
        $this->checkParams($data);

        return new LoginRequest($data['email'], $data['password']);
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkParams(array $data)
    {
        if (!isset($data['email']) || empty($data['email'])) {
            throw new Exception('email required');
        }
        if (!isset($data['password']) || empty($data['password'])) {
            throw new Exception('password required');
        }
    }
}