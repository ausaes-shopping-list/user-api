<?php

namespace User\Application\User\Request;

use Exception;
use User\Domain\Model\User\NewUserRequest;

class GetNewUserRequest
{

    /**
     * @param array $data
     * @return NewUserRequest
     * @throws Exception
     */
    public function build(array $data): NewUserRequest
    {
        $this->checkData($data);

        $newUserRequest = new NewUserRequest(
            $data['email'],
            $data['username'],
            $data['password']
        );
        return $newUserRequest;
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkData(array $data)
    {
        if (!isset($data['email']) || empty($data['email'])) {
            throw new Exception('Email is required');
        }

        if (!isset($data['username']) || empty($data['username'])) {
            throw new Exception('Username is required');
        }

        if (!isset($data['password']) || empty($data['password'])) {
            throw new Exception('Password is required');
        }
    }
}