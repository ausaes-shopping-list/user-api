<?php


namespace User\Application\User;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use User\Application\User\Request\GetNewUserRequest;
use User\Domain\Service\User\NewUserService;

class NewUserController
{
    /**
     * @var GetNewUserRequest
     */
    private $request;
    /**
     * @var NewUserService
     */
    private $service;

    public function __construct(GetNewUserRequest $request, NewUserService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    public function doAction(Request $request)
    {
        try {
            $newUserRequest = $this->request->build($request->request->all());
            $this->service->execute($newUserRequest);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['error' => $e->getMessage()]), 500, [], true);
        }
        return new JsonResponse('{}', 201, [], true);
    }
}