<?php


namespace User\Tests\Integration;


use Exception;
use MongoDB\Client;
use MongoDB\Collection;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use User\Domain\Model\User\User;
use User\Infrastructure\UserRepository\MongoUserRepository;

class MongoUserRepositoryTest extends TestCase
{
    private const MONGO_SCHEMA = 'mongodb://shopping-list.mongodb:27017';
    private const DATABASE = 'test';
    private const COLLECTION = 'users';

    /**
     * @var MongoUserRepository
     */
    private $repository;
    /**
     * @var Client
     */
    private $client;
    /**
     * @var Collection
     */
    private $collection;

    public function setUp()
    {
        $this->client = new Client(self::MONGO_SCHEMA);
        $this->repository = new MongoUserRepository(self::MONGO_SCHEMA, self::DATABASE, self::COLLECTION);
        $this->collection = $this->client->{self::DATABASE}->{self::COLLECTION};
        $this->prepareCollection();
    }


    public function testCanAddNewUser()
    {
        $user = new User('test@email.com', 'password', 'test', 'token');
        $this->repository->addUser($user);

        $collection = $this->client->{self::DATABASE}->{self::COLLECTION};
        $mongoUser = $collection->findOne(['email' => 'test@email.com']);
        $this->assertNotNull($mongoUser);
        $this->assertEquals('test@email.com', $mongoUser['email']);
        $this->assertTrue(password_verify('password', $mongoUser['password']));
        $this->assertEquals('test', $mongoUser['username']);
        $this->assertEquals('token', $mongoUser['token']);
        $this->prepareCollection();

    }

    public function testCanGetUserByEmail()
    {
        $user = $this->repository->getLoginUser('user2@email.com', 'password2');

        $this->assertEquals('user2', $user->getUsername());
        $this->assertEquals('token2', $user->getToken());
        $this->assertTrue(password_verify('password2', $user->getPassword()));

    }

    public function testLoginWithBadEmailThrowsException()
    {
        $this->expectException(Exception::class);
        $this->repository->getLoginUser('user22@email.com', 'password2');
    }

    public function testLoginWithBadPasswordThrowsException()
    {
        $this->expectException(Exception::class);
        $this->repository->getLoginUser('user2@email.com', 'passwordWrong');

    }

    private function prepareCollection(): void
    {
        $this->collection->drop();
        $this->collection->insertMany([
            [
                'email' => 'user1@email.com',
                'token' => 'token1',
                'username' => 'user1',
                'password' => '$2y$10$MMBDZJJ102WUgmPRHVeXJuWFpvvJJoT67djpp6pngV325G34QA2Jq'
            ],
            [
                'email' => 'user2@email.com',
                'token' => 'token2',
                'username' => 'user2',
                'password' => '$2y$10$yGr30o9TR.iWk5yx8NH3beTcY3TpYyBI2JvB6/mEneQEmdBxRnk9y'
            ],
            [
                'email' => 'user3@email.com',
                'token' => 'token3',
                'username' => 'user3',
                'password' => '$2y$10$/61.6Zrt.vOjtFyvRBfKleY3rFdynXTgknDV1BIVlFezDEUXMvkg.'
            ]
        ]);
    }


}