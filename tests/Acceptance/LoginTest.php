<?php

namespace User\Tests\Acceptance;

use MongoDB\Client;
use MongoDB\Collection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginTest extends WebTestCase
{
    private const MONGO_SCHEMA = 'mongodb://shopping-list.mongodb:27017';
    private const DATABASE = 'test';
    private const COLLECTION = 'users';

    protected function setUp()
    {

        $client = new Client(self::MONGO_SCHEMA);
        $collection = $client->{self::DATABASE}->{self::COLLECTION};
        $this->prepareCollection($collection);

    }

    public function testLoginWithBadPasswordReturnsError()
    {
        $client = static::createClient();
        $client->request('POST', '/user/login',
            [
                'email' => 'user1@email.com',
                'password' => 'passwordBad'
            ]
        );
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('error', $response);
        $this->assertEquals('wrong password', $response->error);

    }

    public function testLoginWithBadEmailReturnsError()
    {
        $client = static::createClient();
        $client->request('POST', '/user/login',
            [
                'email' => 'userBad@email.com',
                'password' => 'password1'
            ]
        );
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('error', $response);
        $this->assertEquals('user userBad@email.com not found', $response->error);

    }

    public function testLoginWithoutEmailReturnsError()
    {
        $client = static::createClient();
        $client->request('POST', '/user/login',
            [
                'password' => 'password1'
            ]
        );
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('error', $response);
        $this->assertEquals('email required', $response->error);

    }

    public function testLoginWithoutPasswordReturnsError()
    {
        $client = static::createClient();
        $client->request('POST', '/user/login',
            [
                'email' => 'userBad@email.com',
            ]
        );
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('error', $response);
        $this->assertEquals('password required', $response->error);

    }

    public function testLoginOk()
    {
        $client = static::createClient();
        $client->request('POST', '/user/login',
            [
                'email' => 'user1@email.com',
                'password' => 'password1'
            ]
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('username', $response);
        $this->assertObjectHasAttribute('email', $response);
        $this->assertObjectHasAttribute('token', $response);
    }

    private function prepareCollection(Collection $collection)
    {
        $collection->drop();
        $collection->insertMany([
            [
                'email' => 'user1@email.com',
                'token' => 'token1',
                'username' => 'user1',
                'password' => '$2y$10$MMBDZJJ102WUgmPRHVeXJuWFpvvJJoT67djpp6pngV325G34QA2Jq'
            ],
            [
                'email' => 'user2@email.com',
                'token' => 'token2',
                'username' => 'user2',
                'password' => '$2y$10$yGr30o9TR.iWk5yx8NH3beTcY3TpYyBI2JvB6/mEneQEmdBxRnk9y'
            ],
            [
                'email' => 'user3@email.com',
                'token' => 'token3',
                'username' => 'user3',
                'password' => '$2y$10$/61.6Zrt.vOjtFyvRBfKleY3rFdynXTgknDV1BIVlFezDEUXMvkg.'
            ]
        ]);
    }

}