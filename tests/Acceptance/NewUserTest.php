<?php

namespace User\Tests\Acceptance;

use MongoDB\Client;
use MongoDB\Collection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewUserTest extends WebTestCase
{
    private const MONGO_SCHEMA = 'mongodb://shopping-list.mongodb:27017';
    private const DATABASE = 'test';
    private const COLLECTION = 'users';

    protected function setUp()
    {

        $client = new Client(self::MONGO_SCHEMA);
        $collection = $client->{self::DATABASE}->{self::COLLECTION};
        $this->prepareCollection($collection);

    }

    public function requiredParamsErrorProvider()
    {
        return [
            [['email' => 'user@email.com']],
            [['email' => 'user@email.com', 'username' => 'username', 'password' => '']],
            [['email' => '', 'username' => 'username', 'password' => 'password']],
            [['email' => 'email@email.com', 'username' => '', 'password' => 'password']]
        ];
    }

    public function requiredParamsOkProvider()
    {
        return [
            [['email' => 'email@email.com', 'username' => 'username', 'password' => 'password']]
        ];
    }

    /**
     * @param $params
     * @dataProvider requiredParamsErrorProvider
     */
    public function testAddUserWithoutAllRequiredParamsReturnsError($params)
    {
        $client = static::createClient();
        $client->request('POST', '/user/new',
            $params
        );
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertObjectHasAttribute('error', $response);
    }

    /**
     * @param $params
     * @dataProvider requiredParamsOkProvider
     */
    public function testAddUserWithAllRequiredParamsReturnsOk($params)
    {
        $client = static::createClient();
        $client->request('POST', '/user/new',
            $params
        );
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    private function prepareCollection(Collection $collection)
    {
        $collection->drop();
        $collection->insertMany([
            [
                'email' => 'user1@email.com',
                'token' => 'token1',
                'username' => 'user1',
                'password' => '$2y$10$MMBDZJJ102WUgmPRHVeXJuWFpvvJJoT67djpp6pngV325G34QA2Jq'
            ],
            [
                'email' => 'user2@email.com',
                'token' => 'token2',
                'username' => 'user2',
                'password' => '$2y$10$yGr30o9TR.iWk5yx8NH3beTcY3TpYyBI2JvB6/mEneQEmdBxRnk9y'
            ],
            [
                'email' => 'user3@email.com',
                'token' => 'token3',
                'username' => 'user3',
                'password' => '$2y$10$/61.6Zrt.vOjtFyvRBfKleY3rFdynXTgknDV1BIVlFezDEUXMvkg.'
            ]
        ]);
    }
}